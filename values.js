function values(obj) {
  const resultValue = [];
  for (let value in obj) {
    resultValue.push(obj[value]);
  }
  return resultValue;
}

module.exports = values;
