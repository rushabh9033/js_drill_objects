function pairs(obj) {
  const resultArr = [];
  for (let key in obj) {
    objVal = key + ": " + obj[key];
    resultArr.push(objVal);
  }
  return resultArr;
}

module.exports = pairs;
