function mapObject(obj, cb) {
  const resultObj = {};

  for (let item in obj) {
    resultObj[item] = cb(obj[item]);
  }
  return resultObj;
}

module.exports = mapObject;
