function defaults(obj, defaultProps) {
  Object.keys(defaultProps)
    .map((element) => element)
    .forEach((element) => {
      if (!obj[element]) {
        obj[element] = defaultProps[element];
      }
      //   console.log(el);
      //   console.log(defaults[el]);
    });

  return obj;
}

module.exports = defaults;
