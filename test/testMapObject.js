const mapObject = require("../mapObject.js");

const testObject = { name: "Bruce Wayne", age: 36, location: "Gotham" };

const callBack = (element) => element;
const result = mapObject(testObject, callBack);

console.log(result);
